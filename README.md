# Progetto2015

Funzione Semplice: Val.Err

in ingresso prende una cella in cui si è verificato un avviso di errore.
Verifica che appartenga alla classe degli errori e restituisce un Boolean (true o false) a seconda sia un errore o un #N/D

Funzione Complessa. MATR.INVERSA

prende in ingresso una matrice NxN e restituisce l'inversa.
Calcolandola per mezzo della formula del complemento algebrico