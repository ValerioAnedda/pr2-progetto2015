package it.unica.pr2.progetto2015.g44940;

import java.lang.*;


public class Semplice implements it.unica.pr2.progetto2015.interfacce.SheetFunction {

	
    /** 
    Argomenti in input ed output possono essere solo: String, Integer, Long, Double, Character, Boolean e array di questi tipi.
    Ad esempio a runtime si puo' avere, come elementi di args, un Integer ed un Long[], e restituire un Double[];
    */
	public Object execute(Object... args) 
	{
		String s = args[0].toString();
	 	
	 	if ( isError(s))
			{	
			if (s == "#N/D")
				return true;
			else
				return false;
			}	
		else 
			return false;
	 }
			
	
	/*verifica il tipo di errore tramite la string dell'avviso*/
	private Boolean isError (String arg){
	
		 switch (arg) {
            		case "501":
            		case "502":
            		case "503":
            		case "504":
            		case "508":
            		case "509":
            		case "510":
            		case "511":
            		case "512":
            		case "513":
            		case "514":
            		case "516":
            		case "517":
            		case "518":
            		case "519":
            		case "#VALORE":
            		case "520":
            		case "521":
            		case "522":
            		case "523":
            		case "524":
            		case " #REF":
            		case "525":
            		case "#NOME?":
            		case "526":
            		case "527":
            		case "532":
            		case "#DIV/0!": 
 			case "#N/D":return true;
 			default: return false;           		
		}
	}

    /** 
    Restituisce la categoria LibreOffice;
    Vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    ad esempio, si puo' restituire "Data&Orario" oppure "Foglio elettronico"
    */
    public final String getCategory() {
		return "Informazione";
	}

    /** Informazioni di aiuto */
    public final String getHelp() {
		return "Verifica la presenza di condizioni di errore, tranne il valore di errore #N/D, e restituisce VERO o FALSO.";
	} 

    /** 
    Nome della funzione.
    vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
    ad es. "VAL.DISPARI" 
    */         
    public final String getName() {
		return "VAL.ERR";
	}

}
