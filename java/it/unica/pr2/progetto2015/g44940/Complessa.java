	package it.unica.pr2.progetto2015.g44940;


	import java.awt.geom.AffineTransform;
	public class Complessa implements it.unica.pr2.progetto2015.interfacce.SheetFunction {

	
	    /** 
	    Argomenti in input ed output possono essere solo: String, Integer, Long, Double, Character, Boolean e array di questi tipi.
	    Ad esempio a runtime si puo' avere, come elementi di args, un Integer ed un Long[], e restituire un Double[];
	    */
	    
	    /*
  		1 Prima calcola il valore del determinante della matrice, chiamiamolo det(A) e vediamo se e' diverso da zero; se e' uguale 			a zero non esiste la matrice inversa
    		2 Calcola la trasposta della matrice di partenza (basta scambiare tra loro le righe con le colonne)
   		3 Per ogni elemento della matrice trasposta calcolane il complemento algebrico, considerando il complemento algebrico come 			elemento ottieni una nuova matrice, chiamiamola A' (si chiama matrice aggiunta)
   		4 Adesso dividi la matrice A' per det(A) (cioe' dividi ogni termine per det(A)) e ottieni l'inversa della matrice quadrata 			di partenza

	    */
	    	public Object execute(Object... args) {
			/*
			*/
			Double matricePartenza[][] = (Double[][])args;
			Double matriceRestituire[][] = new Double [args.length][args.length];
			
			/* Punto 1*/
			Double det = determinante(matricePartenza);
			/*punto 2*/
			Double matriceTrasposta[][] = trasposta(matricePartenza);	
			/* punto 3 e 4*/
				for (int i = 0 ; i< matriceTrasposta.length; i++)
					for (int j = 0 ; j< matriceTrasposta.length; j++)
						matriceRestituire[i][j] = calcoloComplementoAlgebrico(matriceTrasposta, j,i)/det;
						
			return  matriceRestituire;
			//return matricePartenza.length;
			
		}
	
		
		/*Calcolo del determinante tramite formula del det della matrice 2x2*/	
		
		
		private Double determinante( Double args[][])
		{
		
			Double matrice[][]= (Double[][])args;
			int i = matrice.length;
			Double det = (Double)0.0;
			if(i == 1)
				det = (Double)matrice[0][0];
			else
				if(i == 2)
				{
					det = (Double)((Double)matrice[0][0] * (Double)matrice[1][1] - (Double)matrice[0][1] * (Double)matrice[1][0]);
				} else
				{
					Double matrice1[][] = new Double[i - 1][i - 1];
					
					for(int k = 0; k < i; k++)
					{
						for(int i1 = 1; i1 < i; i1++)
						{
							int j = 0;
							for(int j1 = 0; j1 < i; j1++)
								if(j1 != k)
								{
									matrice1[i1 - 1][j] = (Double)matrice[i1][j1];
									j++;
								}
						}

						if(k % 2 == 0)
							det += (Double)((Double)matrice[0][k] * (Double)determinante(matrice1));
						else
							det -= (Double)((Double)matrice[0][k] * (Double)determinante(matrice1));
					}
				}
			return det;	
		}
		
		
		/* Funzione che fornisce una matrice dalla matrice nxn una matrice (nj-1)x(n-1) con la sottrazione della riga e della colonna per il calcolo del complementoAlgebrico*/
		private Double[][] cancellaRigaColonna ( Double [][] matrice , int  indiciRighe , int indiciColonne) {
			Double [][] restituire = new Double [matrice.length - 1][matrice [0].length -1];
			int r = 0 , c;
			
			for (int i=0;i<matrice.length;i++) {
				//for (int k=0;k<indiciRighe.length;k++)
					if (i == indiciRighe) {
						
						continue;//passo alla riga successiva e rivaluto la condizione del for
					}
					c = 0;
					for (int j=0;j<matrice [i].length;j++) {
						//for (int z=0;z<indiciColonne.length;z++)
						if (j == indiciColonne) {
							
							continue; //passo alla colonna successiva e rivaluto la condizione del for
						}
						restituire [r][c] = matrice [i][j];
						c++;
					}
					r++;
				}

			return restituire;
		} 
		/*passo di calcolo per il carico algebrico*/	
		private Double calcoloComplementoAlgebrico ( Double args[][] , int i, int j)
		{
			Double segno;
			Double restituire;
			Double matrice[][]= (Double[][])args;
			Double[][] matriceRidotta = new Double [matrice.length-1][matrice.length-1];
			matriceRidotta = cancellaRigaColonna( matrice , j, i);
			/*calcolo segno*/
			if ((i+j)%2==0)
			{ 
				segno = 1.0;
			}
			else {
			segno = -1.0;
			}	
				 restituire = segno*determinante(matriceRidotta);
			return restituire;
		}
		
		private Double[][] trasposta (Double matrice[][]) 
		{
			Double  restituire[][] = new Double [matrice.length][matrice.length];
			for (int i =0; i< matrice.length ; i++)
				for (int j =0; j< matrice.length ; j++)
				{
					restituire[i][j]=matrice[j][i];
				}
			return restituire;	
		}
		
		
		
	    /** 
	    Restituisce la categoria LibreOffice;
	    Vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
	    ad esempio, si puo' restituire "Data&Orario" oppure "Foglio elettronico"
	    */
	    	public final String getCategory() {
			return "Matrice";
		}

	    /** Informazioni di aiuto */
	   	 public final String getHelp() {
			return "Calcola la matrice inversa di una matrice n*n ";
		} 

	    /** 
	    Nome della funzione.
	    vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it 
	    */         
	   	 public final String getName() {
			return "MATR.INVERSA";
		}

	}
	


